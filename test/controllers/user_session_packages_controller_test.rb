require 'test_helper'

class UserSessionPackagesControllerTest < ActionController::TestCase
  setup do
    @user_session_package = user_session_packages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:user_session_packages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user_session_package" do
    assert_difference('UserSessionPackage.count') do
      post :create, user_session_package: { end_at: @user_session_package.end_at, professional_id: @user_session_package.professional_id, session_package_id: @user_session_package.session_package_id, start_at: @user_session_package.start_at }
    end

    assert_redirected_to user_session_package_path(assigns(:user_session_package))
  end

  test "should show user_session_package" do
    get :show, id: @user_session_package
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user_session_package
    assert_response :success
  end

  test "should update user_session_package" do
    patch :update, id: @user_session_package, user_session_package: { end_at: @user_session_package.end_at, professional_id: @user_session_package.professional_id, session_package_id: @user_session_package.session_package_id, start_at: @user_session_package.start_at }
    assert_redirected_to user_session_package_path(assigns(:user_session_package))
  end

  test "should destroy user_session_package" do
    assert_difference('UserSessionPackage.count', -1) do
      delete :destroy, id: @user_session_package
    end

    assert_redirected_to user_session_packages_path
  end
end
