# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
[ "Administrador", "Profissional" ].each do |name|
  UserType.create!(name: name)
end

User.create!(name: "Rodrigo Toledo", email: "rodrigo.toledo@sp.agence.com.br",
  password: "123456", phone: "(33) 3321-4657, (33) 9123-4567",
  user_type: UserType.administrator)

['Pilates', 'Fisioterapia', 'Psiquiatria', 'Psicologia'].each do |name|
  Speciality.create!(name: name)
end

Speciality.all.each do |speciality|
  session_package = SessionPackage.create!(user: User.first, speciality: speciality,
    package_type: 'UNIMED', number_of_sessions: rand(2..10), start_at: Date.tomorrow)
  current_date = session_package.start_at
  session_package.number_of_sessions.times do |new_user_session_package|
    # puts 'akiiii'
    user_session_package = session_package.user_session_packages.build(
      start_at: (current_date+new_user_session_package),
      professional_id: User.last.id,
      )
    user_session_package.end_at = (user_session_package.start_at+1.hour)
    user_session_package.save!
  end
end
