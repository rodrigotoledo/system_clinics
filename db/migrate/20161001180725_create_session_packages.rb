class CreateSessionPackages < ActiveRecord::Migration
  def change
    create_table :session_packages do |t|
      t.date :start_at
      t.integer :number_of_sessions
      t.string :package_type
      t.references :user, index: true, foreign_key: true
      t.references :speciality, index: true, foreign_key: true
      t.references :pathology, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
