class CreateUserSessionPackages < ActiveRecord::Migration
  def change
    create_table :user_session_packages do |t|
      t.datetime :start_at
      t.datetime :end_at
      t.references :session_package, index: true, foreign_key: true
      t.integer :professional_id

      t.timestamps null: false
    end
  end
end
