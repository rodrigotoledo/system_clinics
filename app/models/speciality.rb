class Speciality < ActiveRecord::Base
  has_many :pathologies
  has_many :session_packages

  validates_presence_of :name

  def number_of_session_packages_at_date(date)
    number_of = 0
    self.session_packages.where("start_at >= ?",date).each do |session_package|
      number_of += session_package.user_session_packages.count
    end
    number_of
  end
end
