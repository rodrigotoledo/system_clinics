class UserType < ActiveRecord::Base
  def self.administrator
    UserType.where(name: "Administrador").first
  end
end
