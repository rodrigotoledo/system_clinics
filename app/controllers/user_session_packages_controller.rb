class UserSessionPackagesController < ApplicationController
  before_action :set_user_session_package, only: [:show, :edit, :update, :destroy]

  # GET /user_session_packages
  # GET /user_session_packages.json
  def index
    @user_session_packages = UserSessionPackage.all
  end

  # GET /user_session_packages/1
  # GET /user_session_packages/1.json
  def show
  end

  # GET /user_session_packages/new
  def new
    @user_session_package = UserSessionPackage.new
  end

  # GET /user_session_packages/1/edit
  def edit
  end

  # POST /user_session_packages
  # POST /user_session_packages.json
  def create
    @user_session_package = UserSessionPackage.new(user_session_package_params)

    respond_to do |format|
      if @user_session_package.save
        format.html { redirect_to @user_session_package, notice: 'User session package was successfully created.' }
        format.json { render :show, status: :created, location: @user_session_package }
      else
        format.html { render :new }
        format.json { render json: @user_session_package.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_session_packages/1
  # PATCH/PUT /user_session_packages/1.json
  def update
    respond_to do |format|
      if @user_session_package.update(user_session_package_params)
        format.html { redirect_to @user_session_package, notice: 'User session package was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_session_package }
      else
        format.html { render :edit }
        format.json { render json: @user_session_package.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_session_packages/1
  # DELETE /user_session_packages/1.json
  def destroy
    @user_session_package.destroy
    respond_to do |format|
      format.html { redirect_to user_session_packages_url, notice: 'User session package was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_session_package
      @user_session_package = UserSessionPackage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_session_package_params
      params.require(:user_session_package).permit(:start_at, :end_at, :session_package_id, :professional_id)
    end
end
