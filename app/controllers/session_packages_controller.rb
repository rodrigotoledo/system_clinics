class SessionPackagesController < ApplicationController
  before_action :set_session_package, only: [:show, :edit, :update, :destroy]

  # GET /session_packages
  # GET /session_packages.json
  def index
    @session_packages = SessionPackage.all
  end

  # GET /session_packages/1
  # GET /session_packages/1.json
  def show
  end

  # GET /session_packages/new
  def new
    @session_package = SessionPackage.new
  end

  # GET /session_packages/1/edit
  def edit
  end

  # POST /session_packages
  # POST /session_packages.json
  def create
    @session_package = SessionPackage.new(session_package_params)

    respond_to do |format|
      if @session_package.save
        format.html { redirect_to @session_package, notice: 'Session package was successfully created.' }
        format.json { render :show, status: :created, location: @session_package }
      else
        format.html { render :new }
        format.json { render json: @session_package.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /session_packages/1
  # PATCH/PUT /session_packages/1.json
  def update
    respond_to do |format|
      if @session_package.update(session_package_params)
        format.html { redirect_to @session_package, notice: 'Session package was successfully updated.' }
        format.json { render :show, status: :ok, location: @session_package }
      else
        format.html { render :edit }
        format.json { render json: @session_package.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /session_packages/1
  # DELETE /session_packages/1.json
  def destroy
    @session_package.destroy
    respond_to do |format|
      format.html { redirect_to session_packages_url, notice: 'Session package was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_session_package
      @session_package = SessionPackage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def session_package_params
      params.require(:session_package).permit(:start_at, :session_package_type, :user_id, :speciality_id)
    end
end
