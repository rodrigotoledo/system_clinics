json.array!(@pathologies) do |pathology|
  json.extract! pathology, :id, :speciality_id, :name
  json.url pathology_url(pathology, format: :json)
end
