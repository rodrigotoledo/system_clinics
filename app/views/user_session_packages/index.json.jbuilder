json.array!(@user_session_packages) do |user_session_package|
  json.extract! user_session_package, :id, :start_at, :end_at, :session_package_id, :professional_id
  json.url user_session_package_url(user_session_package, format: :json)
end
